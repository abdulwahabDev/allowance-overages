<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CostCode;
use App\JobDetail;
use App\Project;
use App\Bid;

class CustomerHouseController extends Controller
{

    /**
     * INDEX PAGE
     */
    public function index(){
        return view('pages.home');
    }

    /**
     * ENTERS JOB DESCRIPTION AS COST CODE
     */

    public function create_cost_code(){
        return view('pages.cost_code');
    }

    public function store_cost_code(Request $request){
        // dd($request->all());
        $this->validate($request, [
            'description' => 'required'
        ]);

        $cost_code = new CostCode;
        $cost_code->cc_description = $request->description;
        $cost_code->save();
        //dd($cost_code);
        return redirect('enter-cost-code')->with('success', 'Cost code added successfully.');
    }


    /**
     * ENTER AMOUNTS FOR JOB- CREATE NEW JOB
     */
    public function create_job_allowance(){

        $data = array(
            'cost_codes' => CostCode::all(),
            'projects' => Project::all(),
        );

        return view('pages.job_allowance_form')->with($data);
    }

    public function store_job_allowance(Request $request){
        $this->validate($request, [
            'project_id' => 'required',
            'cost_code_id' => 'required',
            'allowance' => 'nullable|numeric',
            'comment' => 'required',
        ]);

        $allowance = new JobDetail;
        $allowance->jd_project_id = $request->project_id;
        $allowance->jd_cost_code_id = $request->cost_code_id;
        $allowance->jd_allowance = $request->allowance;
        $allowance->jd_comment = $request->comment;
        $allowance->save();

        return redirect('new-job-allowance')->with('success', 'Job with allowance added successfully.');
    }

    public function select_project(){
        return view('pages.table_projects')->with('projects', Project::all());
    }

    /**
     * SEE FULL TABLE - CAN UPDATE ALL THE TRANSACTION BY CLICKING THE PERTICULAR TRANSACTION
     */
    public function show_table_job_allowance($project_id){

        $some = CostCode::all();
        foreach($some as $value){
            $where = array(
                'bid_project_id' => $project_id,
                'bid_cost_code_id' => $value->cc_id,
                'bid_approved' => TRUE,
            );

            $whereInvert = array(
                'bid_project_id' => $project_id,
                'bid_cost_code_id' => $value->cc_id,
                'bid_approved' => FALSE,
            );

            $sum[$value->cc_id] = Bid::where($where)->sum('bid_value');
            $sumInvert[$value->cc_id] = Bid::where($whereInvert)->sum('bid_value');
        }

        $data = array(
            'info' => JobDetail::join_info($project_id),
            'sum' => $sum,
            'sumInvert' => $sumInvert,
            'allowance_sum' => self::allowance_sum($project_id),
            'approved_sum' => array_sum($sum),
            'non_approved_sum' => array_sum($sumInvert),
        );

        // return response()->json(['allowances' => $data]);
        return view('pages.table_job_allowance')->with($data);
    }

    /**
     * BIDDING SYSTEM-APPROVED AND DISAPPROVED BIDS
     */

    public function create_bid(){
        $data = array(
            'cost_codes' => CostCode::all(),
            'projects' => Project::all(),
        );
        return view('pages.enter_bid_form')->with($data);
    }

    public function store_bid(Request $request){
        $this->validate($request, [
            'project_id' => 'required',
            'cost_code_id' => 'required',
            'bid' => 'nullable|numeric',
        ]);

        $bid = new Bid;
        $bid->bid_project_id = $request->project_id;
        $bid->bid_cost_code_id = $request->cost_code_id;
        $bid->bid_value = $request->bid;
        $bid->save();

        return redirect('new-bid')->with('success', 'Bid added successfully.');
    }

    public function show_bid(){
        $data = array(
            'info' => Bid::join_info(), 
        );

        return view('pages.bids_table')->with($data);
    }

    public function approve_bid($bid_id, $bid_status){
        
        if($bid_status == FALSE){
            Bid::where('bid_id', $bid_id)->update(['bid_approved' => TRUE]);
        }else{
            Bid::where('bid_id', $bid_id)->update(['bid_approved' => FALSE]);
        }

        return redirect('show-bid');
    }

    /**
     * GET ALLOWANCE SUM
     */
    public static function allowance_sum($project_id){
        return JobDetail::where('jd_project_id', $project_id)->sum('jd_allowance');
    }

     /**
     * GET APPROVED BID SUM
     */
    public static function approved_bid_sum($project_id){
        return JobDetail::where('jd_project_id', $project_id)->sum('jd_approved_bid');
    }

     /**
     * GET VARIACNE SUM
     */
    public static function variance_sum($project_id){
        return self::allowance_sum($project_id) - self::approved_bid_sum($project_id);
    }

     /**
     * 15 PERCENTS
     */
    public static function fifteen_percent_of_net_overages_per_contract(){
        return self::variance_sum() * 0.15;
    }

    /**
     * ALLOWANCE OVERAGE GRAND TOTAL
     */
    public static function allowance_overage_grand_total(){
        return (self::variance_sum() * 0.15) + self::variance_sum();
    }
}
