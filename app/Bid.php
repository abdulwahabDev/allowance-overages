<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Bid extends Model
{
    public static function join_info(){

        $info = DB::table('bids')
            ->leftJoin('projects', 'bids.bid_project_id', '=', 'projects.p_id')
            ->leftJoin('cost_codes', 'bids.bid_cost_code_id', '=', 'cost_codes.cc_id')
            ->orderBy('projects.p_description', 'desc')
            ->orderBy('cost_codes.cc_description', 'desc')
            ->get();

            //dd($info);

        return $info;
    }
}
