<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class JobDetail extends Model
{
    public static function join_info($project_id){

        $info = DB::table('job_details')
            ->leftJoin('cost_codes', 'job_details.jd_cost_code_id', '=', 'cost_codes.cc_id')
            ->leftJoin('projects', 'job_details.jd_project_id', '=', 'projects.p_id')
            ->where('jd_project_id', $project_id)
            ->get();

        return $info;
    }
}
