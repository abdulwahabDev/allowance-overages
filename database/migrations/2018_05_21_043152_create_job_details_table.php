<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_details', function (Blueprint $table) {
            $table->increments('jd_id');
            $table->Integer('jd_project_id')->nullable();
            $table->Integer('jd_cost_code_id')->nullable();
            $table->mediumInteger('jd_allowance')->default(0)->nullable();
            $table->mediumInteger('jd_pending_bid')->default(0)->nullable();
            $table->mediumInteger('jd_approved_bid')->default(0)->nullable();
            $table->text('jd_comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_details');
    }
}
