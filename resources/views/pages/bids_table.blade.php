@extends('layout.template')

@section('content')

<div class="container">
    <h2>Bids Table</h2>
    <p>The .table-bordered class adds borders to a table:</p>            
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>no#</th>
            <th>Project</th>
            <th>Cost code</th>
            <th>Bid</th>
            <th>Date/Time</th>
            <th>Approve/Disapprove</th>
        </tr>
        </thead>
        <tbody>
        @foreach($info as $value)
            <tr>
                <td>{{$value->bid_id}}</td>
                <td>{{$value->p_description}}</td>
                <td>{{$value->cc_description}}</td>
                <td>{{$value->bid_value}}</td>
                <td>{{$value->created_at}}</td>
                @if($value->bid_approved == FALSE)
                <td><a href='{{url("approve-bid/$value->bid_id/$value->bid_approved")}}' class="btn btn-primary">Approve</a></td>
                @else
                <td><a href='{{url("approve-bid/$value->bid_id/$value->bid_approved")}}' class="btn btn-danger">Disapprove</a></td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@endsection