@extends('layout.template')

@section('content')

<div class="container">
    <h2>Projects</h2>
    <p>The .table-bordered class adds borders to a table:</p>            
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>no#</th>
            <th>Project</th>
            <th>select</th>
        </tr>
        </thead>
        <tbody>
        @foreach($projects as $project)
            <tr>
                <td>{{$project->p_id}}</td>
                <td>{{$project->p_description}}</td>
                <td><a  href='{{url("job-allowance-table/$project->p_id")}}' class="btn btn-warning">Job Allowance Detail</a></td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

@endsection