@extends('layout.template')

@section('content')

<div class="container">
    
<h2> Project: {{$info[0]->p_description}}</h2>
    <p>The .table-bordered class adds borders to a table:</p>            
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>no#</th>
            <th>Cost code</th>
            <th>Allowance</th>
            <th>Pending bids</th>
            <th>Approved bids</th>
            <th>Variance</th>
            <th>Comment</th>
        </tr>
        </thead>
        <tbody>
        @foreach($info as $value)
            <tr>
                <td>{{$value->jd_id}}</td>
                <td>{{$value->cc_description}}</td>
                <td>{{$value->jd_allowance}}</td>
                <td>{{$sumInvert[$value->cc_id]}}</td>
                <td>{{$sum[$value->cc_id]}}</td>
                <td>{{$value->jd_allowance - $sum[$value->cc_id]}}</td>
                <td>{{$value->jd_comment}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <p>Net Allowance: <code>${{$allowance_sum}}</code></p>
    <p>Net Approved bids: <code>${{$approved_sum}}</code></p>
    <p>Net non Approved bids: <code>${{$non_approved_sum}}</code></p>
    <p>Net variance: <code>${{$variance = $allowance_sum - $approved_sum}}</code></p>
    <p>Add 15% to net overages per Contract: <code>${{ $add = 0.15*$variance }}</code></p>
    <p>Allowance Overage Grand Total: <code>${{ $add + $variance }}</code></p>
</div>

@endsection