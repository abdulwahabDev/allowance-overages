@extends('layout.template')

@section('content')

<div class="container">
    <h2>Enter new job and allowance</h2>
    {!! Form::open(['action' => 'CustomerHouseController@store_job_allowance' ,'method' => 'POST']) !!}

    <div class="form-group">
        <label for="text">Select project:</label>
        <select name='project_id' class="form-control" >
            <option value=''>-select project-</option>
            @foreach($projects as $project)
                <option value='{{$project->p_id}}'>{{$project->p_description}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="text">Select cost code:</label>
        <select name='cost_code_id' class="form-control" >
            <option value=''>-select cost code-</option>
            @foreach($cost_codes as $cost_code)
                <option value='{{$cost_code->cc_id}}'>{{$cost_code->cc_description}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="text">Enter allowance:</label>
        <input class="form-control" type="text" placeholder="Enter allowance" name="allowance" />
    </div>

    <div class="form-group">
        <label for="text">Comment:</label>
        <textarea class="form-control" id="text" placeholder="Enter cost code description" name="comment"></textarea>
    </div>
    
    <input type="submit" name="submit_job_allowance" class="btn btn-default" value='Submit' />
    {!! Form::close() !!}
</div>

@endsection