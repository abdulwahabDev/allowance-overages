@extends('layout.template')

@section('content')

<div class="container">
    <h2>Enter Bid</h2>
    {!! Form::open(['action' => 'CustomerHouseController@store_bid' ,'method' => 'POST']) !!}

    <div class="form-group">
        <label for="text">Select project:</label>
        <select name='project_id' class="form-control" >
            <option value=''>-select project-</option>
            @foreach($projects as $project)
                <option value='{{$project->p_id}}'>{{$project->p_description}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="text">Select cost code:</label>
        <select name='cost_code_id' class="form-control" >
            <option value=''>-select cost code-</option>
            @foreach($cost_codes as $cost_code)
                <option value='{{$cost_code->cc_id}}'>{{$cost_code->cc_description}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group">
        <label for="text">Enter bid:</label>
        <input class="form-control" type="text" placeholder="Enter bid" name="bid" />
    </div>

    
    <input type="submit" name="submit_bid" class="btn btn-default" value='Submit' />
    {!! Form::close() !!}
</div>

@endsection