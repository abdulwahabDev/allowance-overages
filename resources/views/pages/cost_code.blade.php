@extends('layout.template')

@section('content')

<div class="container">
    <h2>Enter Cost Code</h2>
    {!! Form::open(['action' => 'CustomerHouseController@store_cost_code' ,'method' => 'POST']) !!}

      <div class="form-group">
        <label for="text">Cost code description:</label>
        <textarea class="form-control" id="text" placeholder="Enter cost code description" name="description"></textarea>
      </div>

      <input type="submit" name="submit_cost_code" class="btn btn-default" value='Submit' />
      {!! Form::close() !!}
  </div>

@endsection