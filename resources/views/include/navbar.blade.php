<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
        <a class="navbar-brand" href='{{url("/")}}'>Allowance & overages</a>
        </div>
        <ul class="nav navbar-nav">
        <li class="active"><a href='{{url("/")}}'>Home</a></li>
        <li><a href='{{url("enter-cost-code")}}'>New cost code</a></li>
        <li><a href='{{url("new-job-allowance")}}'>New Job allowance</a></li>
        <li><a href='{{url("new-bid")}}'>New Bid</a></li>
        <li><a href='{{url("show-bid")}}'>Bid table</a></li>
        <li><a href='{{url("project-table")}}'>Job allowance table</a></li>
        </ul>
    </div>
</nav>