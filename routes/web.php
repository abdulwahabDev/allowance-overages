<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CustomerHouseController@index');

Route::get('/enter-cost-code', 'CustomerHouseController@create_cost_code');
Route::post('/store-cost-code', 'CustomerHouseController@store_cost_code');

Route::get('/new-job-allowance', 'CustomerHouseController@create_job_allowance');
Route::post('/store-job-allowance', 'CustomerHouseController@store_job_allowance');

Route::get('/new-bid', 'CustomerHouseController@create_bid');
Route::post('/store-bid', 'CustomerHouseController@store_bid');

Route::get('/show-bid', 'CustomerHouseController@show_bid');

Route::get('/approve-bid/{bid_id}/{bid_status}', 'CustomerHouseController@approve_bid');

Route::get('/project-table', 'CustomerHouseController@select_project');

Route::get('/job-allowance-table/{p_id}', 'CustomerHouseController@show_table_job_allowance');